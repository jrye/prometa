# TODO

* Check if a Zenodo URL can be retrieved via the [Zenodo REST API](https://developers.zenodo.org/)
* Maybe add support for uploading packages to PyPI (based on release tags).
* Idem for uploading archives to Zenodo (with a generated .zenodo.json file).
