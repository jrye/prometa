#!/usr/bin/env python
"""
ORCID functions.
"""


def get_orcid_url(orcid):
    """
    Get the ORCID URL for the given ID.
    """
    return f"https://orcid.org/{orcid}"
