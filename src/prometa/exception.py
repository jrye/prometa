#!/usr/bin/env python
"""
Exceptions.
"""


class PrometaException(Exception):
    """
    Base class for custom exceptions.
    """
